---
title: Comunicación Nº17
subtitle: 'Plan de abordaje integran ante la emergencia sanitaria.'
date: 2020-04-21T17:00
---

### RÍO CEBALLOS. COMISIÓN PARA LA EVALUACIÓN DE LA EMERGENCIA SANITARIA COVID–19

### NUEVAS MEDIDAS DE SEGURIDAD PARA LA AMPLIACIÓN DE ACTIVIDADES

En el marco de lo que dispuso el Gobierno de la Nación a través de la Decisión Administrativa N° 524/2020, en relación a nuevas excepciones al cumplimiento del “Aislamiento Social, Preventivo y Obligatorio”, el Municipio a través de DECRETO N°201/2020 establece, autoriza y regula los siguientes aspectos, a partir del 21 de abril:

✅ La Municipalidad de Río Ceballos abre sus puertas para el cobro de Tributos Municipales y deudas vencidas, como así también para la presentación de Declaración Jurada de la Actividad Comercial, Industrial y de Servicios, de 9 a 13 horas.
✅ La Cooperativa de Obras y Servicios Río Ceballos Ltda. podrá abrir su sede, con una guardia mínima para el cobro de facturas, de lunes a viernes, de 9 a 13 horas.
✅ Los comercios exceptuados por la Decisión Administrativa 524/2020 de la Jefatura de Gabinete de Ministros de la Nación, podrán realizar sus ventas a través de plataformas de comercio electrónico, venta telefónica y otros mecanismos que no requieran contacto personal con clientes y únicamente mediante la modalidad de entrega a domicilio, de lunes a sábados en el horario de 10 a 18 horas, con los debidos resguardos sanitarios, protocolos y planificación de la logística. ⚠️En ningún caso los comercios mencionados podrán abrir sus puertas al público.
✅ Los establecimientos de cobranza de servicios e impuestos podrán abrir sus sedes, de lunes a viernes en el horario de 09 a 16 horas.
✅ La Actividad registral nacional y provincial con sistemas de turnos y guardias mínimas, de Lunes a Viernes de 09 a 13 hs.
✅ La Atención médica y odontológica programada; laboratorios de análisis clínico; centros de diagnóstico por imagen; ópticas podrán atender con sistema de turno de lunes a viernes en el horario de 10 a 18 hs.  
✅ Los Peritos y liquidadores de siniestros de las compañías aseguradoras que permitan realizar tramites y pagos de forma virtual.

Cada local incluido en estos puntos deberá:
👉🏾 Implementar estrictamente las Medidas Preventivas Covid-19 de acuerdo a las recomendaciones de Bioseguridad según Protocolo Provincial.
👉🏾 No exceder un máximo de tres personas (incluido el delivery designado).
👉🏾 Tramitar la Declaración Jurada de Excepción para Circulación en Emergencia Sanitaria – COVID 19 de acuerdo a normativa vigente en la página del Gobierno Nacional.-
https://formulario-ddjj.argentina.gob.ar/certificado/argentino/motivo.
