---
title: Comunicación Nº5
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-16T08:00
---

## En el marco de las acciones de evaluación de la emergencia sanitaria y de las medidas permanentes para la prevención del virus COVID–19, la Comisión informa: 

1. Se realizó esta mañana una reunión entre la Comisión y los equipos sanitarios del Sanatorio Privado del Interior y el servicio de emergencia CEC. Allí se actualizaron los acuerdos y protocolos de procedimientos para la asistencia de consultas, casos con sintomatología, casos sospechosos y derivaciones.  
Entre los principales puntos acordados estuvieron:
    * La importancia de recibir las consultas y realizar en cada caso una minuciosa evaluación para detectar si se trata de un caso epidemiológico que requiere evaluación, seguimiento y derivación.  Ese procedimiento, que requiere de un protocolo de preguntas al paciente, se ejecuta con recepcionistas capacitados y con una minuciosidad que se replica en todos los posibles puntos de recepción. La identificación del tipo de caso resulta central para no desbordar la demanda y para atender de manera pertinente cada cuadro.
    * La triangulación entre el Centro de Salud, el Hospital de Unquillo y el CEC para la derivación de casos sospechosos. 
    * La recepción y evaluación en condiciones de aislamiento en el Sanatorio Privado del Interior, de los casos que acudan allí. 
    * El protocolo de actuación y medidas de seguridad del personal sanitario. 
    * La necesidad de compartir protocolos con otros agentes públicos, como la Policía, para coordinar procedimientos en caso de denuncias. 
2. Se realizó una conferencia de prensa con presencia de comunicadores y comunicadoras de la zona exclusivamente, con las medidas de seguridad pertinente. Allí el Intendente, el Director de Salud y los referentes de las dos instituciones privadas de asistencia, dieron a conocer las medidas adoptadas desde la semana pasada, el estado de situación local y los procedimientos de articulación entre instituciones.
3. Hoy fueron dadas de alta las dos pacientes de Río Ceballos internadas y analizadas en el Hospital de Unquillo, con resultado negativo de  COVID–19. Hasta ahora no se registran otros casos sospechosos. 
4. Hoy comenzó a implementarse el cese de actividades en CCDI, y la entrega de viandas a niños y niñas que asisten a los centros. 
Seguimos activos para disminuir el impacto del virus y evitar su propagación. Las medidas tienen que involucrar a toda la comunidad, en una misma acción conjunta y solidaria, donde nos cuidamos mutuamente. Eso incluye el cuidado personal, el aislamiento, la disminución de todas las actividades sociales y públicas, y el manejo responsable de la información, para no enturbiar los procedimientos. 
Comunidad activa para el cuidado de la salud.