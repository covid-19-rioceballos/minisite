---
title: Comunicación Nº12
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-31T08:49
---

**La Comisión comunica a la comunidad los datos y medidas más recientes:**

1. **Se prorrogan las medidas sanitarias** de aislamiento, el receso administrativo municipal y todas las restricciones establecidas para las distintas actividades en la ciudad, hasta el 12 de abril, conforme a lo establecido por Decreto Nacional 297/2020

2. **Vigilancia Epidemiológica**
   Hasta el momento:
   * 127 personas se encuentran efectivamente en Aislamiento Preventivo Obligatorio por haber arribado al país en los últimos 14 días y han sido notificadas directamente. 
   * 69 de esas personas arribaron de países con circulación viral.
   * 105 personas ya cumplieron el plazo de 14 días de aislamiento obligatorio posteriores a su arribo.
   * 9 personas han sido detenidas por no cumplir con la cuarentena decretada.
   * No hay casos de COVID-19 confirmados en Río Ceballos.


3. **Medidas económicas** 
   * Se dispuso por ordenanza una **partida presupuestaria** específica para las medidas del Plan de Emergencia Sanitaria. 
   * Se prorrogaron los plazos de vencimientos impositivos. 
   * Se habilitaron nuevos **medios de pago electrónico** para facilitar el pago de impuestos sin salir de casa. 
   * Anticipo del **pago de haberes para los empleados** de Planta Permanente y Contratados, con el propósito de acompañar a las familias y aportar alivio a la economía local.


4. **Medidas sociales, de asistencia, promoción, acompañamiento y contención**
   * Se inició la **entrega de módulos alimentarios** (módulo refuerzo nutricional, módulo diabéticos, módulo celíacos, módulo de ayuda social), a través de la Dirección de Equidad y Desarrollo Social, para familias y personas que necesitan ayuda social, en el marco del aislamiento social, preventivo y obligatorio, con el propósito de asegurar la cobertura de las necesidades nutricionales básicas. Las personas asistidas son parte del Registro de Situaciones Críticas relevado por la municipalidad, que es actualizado en forma permanente con datos nacionales y provinciales para conseguir mayor equidad en la distribución de los recursos y garantizar que ningún vecino quede desamparado en esta situación.  
   * Se puso a disposición una **línea telefónica de ayuda social** para recibir consultas y acompañar a la ciudadanía en el acceso a los programas y medidas dispuestos por el Gobierno Nacional y provincial, facilitando los trámites de ANSES y la resolución de formularios. Para solicitar ayuda social comunicarse al **03543-155 72682**.


5. **Controles en la vía pública**
Para acompañar y asegurar el  cumplimiento del aislamiento social, preventivo y obligatorio establecido por el Decreto 297/2020, se realizan controles permanentes y sorpresivos en distintos puntos de la ciudad de Río Ceballos, conforme a la planificación estratégica diagramada por el Distrito Policial XIII, dependiente de la Dirección de Seguridad Sierras Chicas. 


6. **Inspecciones y relevamiento de precios**
El Municipio recorre los comercios para constatar el cumplimiento de los precios máximos y evitar aumentos en el marco de la emergencia sanitaria. Se concretaron inspecciones en ocho supermercados de la ciudad para evitar arbitrariedades y sobreprecios en días de aislamiento obligatorio. Los controles, a cargo de personal municipal, utilizan todas las medidas de seguridad recomendadas en la emergencia sanitaria, y estarán visitando todos los comercios exceptuados del aislamiento en los próximos días.