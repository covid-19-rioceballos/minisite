---
title: Comunicación Nº9
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-20T16:00
---

## Reunión para aplicación del DNU de Aislamiento social y preventivo

![Reunión para aplicación del DNU de Aislamiento social y preventivo](/static/images/uploads/comunicacion-n9-20-03-2020.jpg)

En horas de la mañana se realizó la reunión de coordinación para la aplicación del **Decreto de Necesidad y Urgencia N° 297 “Aislamiento social y preventivo”** entre las autoridades municipales, de Defensa Civil y referentes de las fuerzas de seguridad con sede en Río Ceballos de la Policía de la Provincia de Córdoba y de Gendarmería Nacional.

Durante el encuentro, el Director de Salud, Dr. Alejandro Racca, informó sobre las acciones que se están llevando adelante desde la Comisión para la evaluación de la Emergencia Sanitaria. Por su parte, los referente de las instituciones de seguridad de provincia y nación informaron sobre los operativos de control que llevan adelante en nuestra localidad.

Además, se pusieron en común los protocolos de actuación existentes para el abordaje de ciudadanos o ciudadanas que se encuentren incumpliendo las medidas de aislamiento preventivo para el COVID-19 (Coronavirus), como así también casos que puedan a llegar a surgir en personas que hayan arribado recientemente de los países de circulación del virus.

Los presentes destacaron la disposición y voluntad de la ciudadanía a colaborar y reconocieron que han estado informando, junto a la GUM, a comerciantes que se encuentran fuera del Artículo 6 de dicho DNU, para que cierren sus puertas.