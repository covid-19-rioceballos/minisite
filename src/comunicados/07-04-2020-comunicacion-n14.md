---
title: Comunicación Nº14
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-04-07T12:45
---

La Comisión comunica a la comunidad los datos y medidas más recientes: 

1. **COE: Centro de Operaciones de Emergencia**
Quedaron conformados ocho COE regionales en toda la provincia. Dependen del Poder Ejecutivo provincial y tienen la función de asistir a la población en cada territorio, planificando estrategias coordinadas y complementarias en toda una región para la  mitigación de la pandemia en materia de salud, seguridad y acción social. Agrupan a autoridades provinciales, municipales, legislativas, organizaciones civiles, fuerzas de seguridad.
El Municipio de Río Ceballos quedó integrado al COE Central que funciona en el complejo Pablo Pizzurno y reúne a las localidades del Gran Córdoba.  Participan de esta instancia el Intendente,  el Director de Salud y Referentes de Defensa Civil. 

2. **Vigilancia Epidemiológica**
    * A partir de la conformación de los COE y en virtud de la circulación de virus en conglomerados urbanos como Córdoba, Alta Gracia y Río Cuarto, se ha ampliado la **definición de Caso Sospechoso**. Se suman como factores para determinarlo las personas con diagnóstico de Neumonía y el Personal de Salud con fiebre y síntomas respiratorios.
    * Hasta el momento se registran **12 casos positivos en el Departamento Colón**:
        7 en Villa Allende
        2 Col Caroya
        1 Cerro Azul
        1 Mendiolaza
        1 La Calera
    * No hay casos de COVID-19 confirmados en Río Ceballos

3. **Medidas Sanitarias**
Continúa el aislamiento y los controles para evitar la circulación. 
Se recomienda a la población respetar la medida de cuarentena de manera estricta en estos días en que está previsto el aumento de contagios.

4. **Plan de abordaje integral: información, herramientas para la organización comunitaria y ayuda social**
Se creó un sitio para que la población pueda buscar información sobre las medidas de emergencia sanitaria y solicitar u ofrecer ayuda para enfrentar las distintas situaciones que resultan del contexto de aislamiento. http://coronavirus.rioceballos.gob.ar/
    * **Necesito ayuda social**.  Se ofrece un formulario para conocer la situación y las necesidades específicas y orientar el acompañamiento y ayuda a cada familia o persona. 
    * **Puedo ayudar**. Para quienes puedan realizar trabajos voluntarios.
    * **Soy trabajador informal**. Para quienes necesiten herramientas para ofrecer su producto o servicio.
    * **Pro.Co.E. Programa Comunitario de Emergencia**.  Fue creado a través del decreto 174/20, para generar un fondo de acción eficaz y transparente, que permita llegar a cada rincón de Río Ceballos.
El mismo está conformado con fondos de partidas municipales, aportes por la reducción salarial de funcionarios municipales, ayudas de vecinos y asociaciones civiles, clubes y empresarios locales.  Para colaborar con mercadería o dinero, la población debe comunicarse con el Cel 📱 [351 7657797](tel:3517657797).

5. **Autorización para prestar servicios a Jardineros/as y pileteros/as**
Por decisión administrativa nacional 450/2020, podrán volver a realizar tareas domiciliarias durante el aislamiento social, preventivo y obligatorio.  La decisión se basa en la importancia de sostener medidas de prevención contra la proliferación de mosquitos que puedan transmitir dengue. 
☎️ Para hacer la autorización deben comunicarse VÍA WHATSAPP con el Cel.: [354 351-6387](tel:3543516387) a los fines de obtener el protocolo de seguridad y el permiso correspondiente.