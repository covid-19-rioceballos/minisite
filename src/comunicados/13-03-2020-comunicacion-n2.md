---
title: Comunicación Nº2
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-13T14:00
---

## Medidas:

Desde tempranas horas de la mañana, el Comité de Salud del municipio está tomando medidas de bioseguridad para avanzar en condiciones higiénicas de prevención y evitar las enfermedades respiratorias. Además, se está compartiendo información con comercios, geriátricos e instituciones educativas para acercarles las pautas oficiales de prevención de enfermedades respiratorias. 

El Comité de Salud estuvo reunido con todo el personal de la Dirección de Salud para la implementación de medidas de aislamiento y aplicación de protocolo ante la presencia de casos que revistan sospecha.

Desde el municipio se desaconseja la concurrencia de vecinos y vecinas al Centro de Salud Norcelo Cardozo para consultas de personas sanas, turnos programados y cualquier otra patología que no revista urgencia. 

**Frente a la aparición de fiebre y/o cuadro respiratorio, con antecedentes de viajes al exterior, o contacto con caso sospechoso o confirmado de COVID-19 o Coronavirus, llamar al teléfono 03543 - 457485, disponible las 24 horas.**

**Sólo para la notificación de personas que hayan arribado al país en los últimos 14 días, o contacto con caso sospechoso o confirmado de COVID-19 o Coronavirus, está disponible el el teléfono 03543 -15571743 de 8 a 20 horas.**

Actualmente hay dos personas de nuestra localidad que se encuentran en aislamiento hospitalario preventivo. Una de ellas tiene antecedentes de viaje al exterior y la otra es su conviviente, ambas con solo síntomas leves respiratorios, a quienes se les está realizando los procedimientos diagnósticos indicados para el descarte o confirmación de la enfermedad. En coordinación con el Equipo del Hospital de Unquillo estamos aguardando los resultados y comunicados por el seguimiento del estado clínico de ambas, que por el momento no revisten de gravedad.


### Centro de Salud. Se define: 
	
- **Adecuación del servicio de salud** se encuentra vigente el el protocolo de abordaje clínico de caso sospechoso. 

- **Prioridad de atención de casos sospechosos.** Los casos se evalúan y si responden a síntomas compatibles y condiciones de referencia, se derivan al Hospital de Unquillo con el Servicio de Emergencia. 

- **Se aconseja no concurrir al Centro de Salud por turnos programados y asistencias por cuadros que no sean de emergencia.** Se recomienda no acudir por controles de persona sana. Para ello sugerimos la utilización de los servicios de las cuatro Postas Sanitarias, donde además se atiende normalmente. Éstas comunicarán al Centro de Salud en caso de recibir casos o notificaciones relativas a COVID–19.

**Medidas y recomendaciones para todas las instituciones y organizaciones que tienen atención al público
Se implementa el protocolo de recomendaciones para la prevención de infecciones respiratorias en empresas y organismos con atención al público del Ministerio de Salud de la Nación, que incluye:**

**Disponibilidad de alcohol en gel, agua y jabón para el lavado de manos.
Lavarse frecuentemente con agua y jabón o utilizar soluciones a base de alcohol y secarse con toallas descartables o secador por aire.
Cubrirse la nariz y la boca con el pliegue interno del codo o usar un pañuelo descartable al estornudar o toser.
Adaptación de recipientes de basura para evitar el contacto físico
Toser en el pliegue del codo.
Ventilar los ambientes.
Limpiar frecuentemente las superficies y los objetos que se usan con frecuencia.
Evitar el contacto con superficies expuestas en lugares públicos** (tachos de residuos, mostradores, etc.).

### Escuelas

**Los establecimientos educativos deben cumplir con los mismos requerimientos que el resto de las instituciones. Se les acercará un listado de recomendaciones e insumos para la correcta higiene personal** para la atención de enfermedades respiratorias. Por el momento queda suspendido el Cronograma para la realización del Certificado Único de Salud.  

### Comunicación y difusión de información

**Se define como voceros oficiales para brindar información desde el Municipio al Dr. Alejandro Racca, Director de Salud, y al Sr. Intendente Eduardo Baldassi.** Quienes informarán de manera diaria la situación y medidas adoptadas, en consonancia con lo que se vaya definiendo con las autoridades sanitarias provinciales y nacionales. 

Se desaconseja replicar difundir información que no sea oficial. Debemos ser solidarios y responsables.