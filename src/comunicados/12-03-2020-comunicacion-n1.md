---
title: "Comunicación Nº1"
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-12T16:00
---

El Municipio de Río Ceballos conformó la Comisión para la evaluación de la Emergencia Sanitaria con el propósito de organizar y coordinar las medidas correspondientes en virtud de la situación epidemiológica del  COVID–19 (coronavirus)

Como primeras medidas adoptadas desde el Municipio, hasta la actualización del protocolo provincial, se resuelve:

* Suspender de todas las actividades y eventos públicos, masivos o grupales organizados por el municipio. 
* Recomendar  suspender actividades de concurrencia masiva a todas las organizaciones y particulares que tengan eventos programados.
* Adecuar el servicio de salud de la localidad según el protocolo de abordaje clínico de caso sospechoso. 
* Establecer una línea de Atención Telefónica de 8 a 20hs 03543 15571743 como contacto SOLA Y EXCLUSIVAMENTE ante notificación de personas con antecedentes de viajes a zonas de circulación viral y casos sospechosos.
    * Zonas de Circulación Viral según reporte Epidemiológico del 11 de Marzo del 2020 son: China, Corea del Sur, Japón, Irán, Europa y Estados Unidos – (y los que en el futuro incluya el Ministerio de Salud de la Nación).
    * Caso Sospechoso: personas con fiebre y/o cuadro respiratorio, más:
        * Antecedentes de viaje a China, Italia, Corea del Sur, Japón, Irán,  España, Alemania,     Francia y Estados Unidos en los últimos 14 días, y/ó
        * Contacto estrecho de casos sospechoso o confirmado de COVID-19.

* Recomendar a todo vecino o vecina que haya llegado del exterior el aislamiento domiciliario por el plazo de 14 días desde el arribo al país y concurrir al Centro de Salud u Hospital de Referencia Regional (Hospital de Unquillo) ante la presencia de Fiebre y/o cuadro respiratorio luego del arribo o contacto con caso sospechoso o confirmado de COVID-19.

Todas las medidas serán actualizadas el viernes 13 a partir de la coordinación con el Ministerio de Salud de la Provincia.