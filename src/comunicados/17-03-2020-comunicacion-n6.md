---
title: Comunicación Nº6
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-17T08:00
---

## Se informa que:

1. Desde ayer se suman al listado de países de riesgo por circulación viral de COVID- 19 **Chile y Brasil** (además de **China - Corea del Sur - Japón - Irán - Europa - Estados Unidos**). Por lo tanto las personas que hayan arribado desde esos países (cualquiera sea la ciudad de la que vienen) deberán **permanecer en aislamiento**❗ cumpliendo la cuarentena por 14 días, de acuerdo a las disposiciones nacionales, provinciales y locales. 
2. Para notificación de personas que arribaron de alguno de esos países durante los últimos 14 días  llamar al  ☎ **03543 -15571743** de 8 a 20 hs.
3. Ante la presencia de fiebre y/o síntomas respiratorios, en casos que estén cumpliendo aislamiento, hayan llegado de países de riesgo o hayan tenido contacto con casos sospechosos o confirmados de COVID-19 llamar al ☎ **03543 – 457485**
4. Se está llevando a cabo una reunión entre los Intendentes del Gran Córdoba y sus equipos de salud, para acordar medidas coordinadas en toda el área metropolitana, a partir de lo cual se actualizarán las disposiciones en cada municipio.