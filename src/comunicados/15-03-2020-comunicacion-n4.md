---
title: Comunicación Nº4
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-15T11:00
---

## Suspensión de Actividades (CCDI, Campo de Deportes).

En el marco de la emergencia sanitaria y en virtud de los anuncios realizados por el Gobierno Nacional para evitar la propagación del  virus COVID - 19, la COMISIÓN PARA EVALUAR LA EMERGENCIA SANITARIA de Río Ceballos resuelve la implementación de nuevas medidas tendientes a reducir los riesgos y la circulación del virus. 

1. **La suspensión de las actividades en los Centros de Cuidado y Desarrollo Infantil (CCDI) hasta el 31 de marzo.** 

☎ Por consultas comunicarse con los CCDI a los teléfonos:

📌 Illia:451507
📌 Sonrisas de Miel: 451313
📌 Alfredo Cavalotti: 450303
📌 San Francisco: 450304
📌 Luna de Coco: 03543 155 72356

2. **La suspensión de todas las actividades deportivas que se realizan en el Campo de Deportes Jorge Ñewbery, hasta el 31 de marzo.**

☎ Por consultas comunicarse 450305. 

3. **La realización de una conferencia de prensa a cargo del Intendente y el Director de Salud para dar a conocer información detallada sobre la situación local y las medidas implementadas. Lunes 16 de Marzo, 11 hs., Centro Comercial, Industrial y Turístico de Río Ceballos; Av. San Martín 4431.**

4. **El refuerzo de todas las medidas de prevención en cada una de las dependencias municipales y en la comunidad.**