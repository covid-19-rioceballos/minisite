---
title: Comunicación Nº13
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-04-01T17:45
---

La Comisión comunica a la comunidad los datos y medidas más recientes: 

1. **Ordenanza Nro 2700/2020 Convenio con el Ministerio de Industria, Comercio y Minería de la Provincia de Córdoba para el control de precios**
El Concejo Deliberante autoriza al DEM a suscribir un convenio por el cual el Ministerio delega las funciones operativas para las **intervenciones en defensa de los derechos y el interés general de consumidores y usuarios**. En el marco de este acuerdo, el Municipio dará continuidad a los controles de precios, con capacidad de elaborar actas en los casos en que no se respeten los precios máximos de referencia establecidos por la Nación en el contexto de emergencia sanitaria. Esta medida apunta a cuidar las economías de cada familia y evitar la suba abusiva de precios. 
2. **Ordenanza 2701/2020 Descuento en el salario de funcionarios y funcionarias**
El Concejo Deliberante aprobó el proyecto de ordenanza por el cual se aplicará un descuento del 30% al básico del salario en bruto del mes de Abril de 2020, para los cargos de autoridades superiores de la planta política municipal. El resultante del descuento será incorporado a la partida presupuestaria establecida para la emergencia sanitaria. 
3. **Acompañamiento y control de las medidas de aislamiento** 
Para acompañar y asegurar las medidas de aislamiento social, preventivo y obligatorio continúan los controles en distintos puntos de nuestra ciudad. El personal policial y de la Guardia Urbana Municipal llevan adelante operativos tendientes a concientizar y favorecer el cumplimiento del decreto de aislamiento.  En los casos en que se están violando las disposiciones, sin justificativo que lo acredite, se efectúa la retención de vehículos y la detención de personas.