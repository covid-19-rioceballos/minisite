---
title: Comunicación Nº3
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-13T14:00
---

## Adhesión a Decreto Provincial:

La Municipalidad de Río Ceballos informa:

En el marco de la grave situación sanitaria en nuestro país, la cual ha incrementado su intensidad en las últimas horas, la Municipalidad de Río Ceballos, por [Decreto N° 147/2020](https://drive.google.com/open?id=1PsX_0zT0aTgrWVtizWKq6CKLqZYNTEXt), adhiere al Decreto Nº 190/2020 del Gobierno de la Provincia de Córdoba en todos sus términos.

La disposición provincial establece como medida principal el Estado de Alerta y Prevención Sanitaria y la Suspensión en todo el territorio de la Provincia de Córdoba de la realización de eventos públicos o privados con concentración masiva de personas de la naturaleza que fueren, social, cultural, académicos, de capacitación, artísticos, deportivos, quedando en consecuencia sin efecto ni valor alguno las habilitaciones que hubieren otorgado los organismos provinciales de la administración central o descentralizada a esos efectos.