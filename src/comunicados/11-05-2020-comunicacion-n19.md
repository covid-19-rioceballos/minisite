---
title: Comunicación Nº19
subtitle: 'Plan de abordaje integran ante la emergencia sanitaria.'
date: 2020-05-11T19:30
---

### En relación a la Emergencia Sanitaria la Comisión Informa:

**Medidas administrativas**

**Decreto Nro. 231/2020 (10/05/2020)**. Prorroga hasta el día 24 de Mayo de 2020 inclusive las medidas sanitarias establecidas a nivel local en concordancia con las disposiciones nacionales y provinciales, por las cuales se establece la extensión del aislamiento social, Preventivo y Obligatorio con el fin de contener y mitigar la propagación de la epidemia de COVID-19 y preservar la salud pública. En tal sentido el decreto PROHIBE, en el ejido de la ciudad de Río Ceballos, las siguientes actividades:

-   Dictado de clases presenciales en todos los niveles y todas las modalidades;
-   Eventos públicos y privados: sociales, culturales, recreativos, deportivos, religiosos y de cualquier otra índole que implique la concurrencia de personas;
-   Centros comerciales, cines, teatros, centros culturales, bibliotecas, museos, restaurantes, bares, gimnasios, clubes y cualquier espacio público o privado que implique la concurrencia de personas;
-   Transporte público de pasajeros interurbano, interjurisdiccional e internacional, salvo para los casos previstos en el Art. 11º del DNU 459/2020;
-   Actividades turísticas; apertura de parques, plazas o similares.

**DECRETO N° 232/2020. (10/05/2020)**. Dispone, en razón de la autorización emitida por el Gobierno de la Provincia de Córdoba, a través del C.O.E. en el ejido de la ciudad de Río Ceballos, la habilitación y ejercicio de las siguientes Profesiones: Escribanos, Contadores, Abogados, Arquitectos, Ingenieros, Agrimensores, Corredores Inmobiliarios, Gestores Matriculados y Martilleros, Oftalmólogos, Kinesiólogos, Fisioterapeutas, Fonoaudiólogos, Psiquiatras, Psicólogos y Nutricionistas, como así también la habilitación de Obras Privadas, Empresas e Industrias y Comercios, en el marco de los Protocolos Correspondientes puestos a disposición a través de medios oficiales y página web del municipio.

-   Se pusieron a disposición los protocolos de bioseguridad para la implementación de las medidas establecidas en los decretos, incluyendo la actividad de Quinielas y Peluquerías.

Se solicita a toda la comunidad respetar y hacer respetar las medidas de seguridad vigentes: distanciamiento social, uso de barbijos y tapabocas en la vía pública, medidas de higiene, circulación con permisos correspondientes y movilizarse sólo para cubrir las necesidades esenciales en comercios de cercanía.

Más info: https://coronavirus.rioceballos.gob.ar/fase-4/

_Comunidad y Municipio responsables_
