---
title: Comunicación Nº16
subtitle: 'Plan de abordaje integran ante la emergencia sanitaria.'
date: 2020-04-19T22:00
---

### RÍO CEBALLOS. COMISIÓN PARA LA EVALUACIÓN DE LA EMERGENCIA SANITARIA COVID–19

**Información Epidemiológica**

El Área de Epidemiología de la Provincia informa el día de hoy que registra una persona con resultado POSITIVO para COVID-19 con domicilio en Río Ceballos pero que NO reside en la localidad desde antes del inicio del Aislamiento Social Preventivo y Obligatorio. De esta manera el Reporte Epidemiológico contabilizará un caso positivo correspondiente a Río Ceballos por su domicilio postal pero que se encuentra en la actualidad en otra localidad.

El Municipio ya está trabajando de manera articulada con el Área de Epidemiología de la Provincia para mantener actualizada la información y está a disposición para las acciones de Vigilancia Epidemiológica que fuesen necesarias.

En el Dpto. Colón hasta el día de la fecha 19/04/2020 hay CONFIRMADOS 81 casos de COVID-19 de los cuales:

54 corresponden a la localidad de Saldán relacionados a la residencia geriátrica; 13 a Villa Allende; 6 a Mendiolaza; 3 a La Calera; 2 a Colonia Caroya; 1 a Cerro Azul; 1 a Salsipuedes y 1 a Río Ceballos **(que no reside en la localidad desde antes del Aislamiento Social Preventivo y Obligatorio)**.

Informate en [http://coronavirus.rioceballos.gob.ar/](http://coronavirus.rioceballos.gob.ar/)
