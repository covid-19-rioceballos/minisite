---
title: Comunicación Nº11
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-24T16:00
---

En el marco de la emergencia sanitaria nacional y como parte de las medidasque la  COMISIÓN viene implementando, se realizó hoy una reunión convocada por el Ejecutivo Municipal, de la que participaron los funcionarios del DEM, el Director de salud, los integrantes del Concejo Deliberante, la Guardia Urbana (GUM), Defensa Civil, Gendarmería y Policía.
En el encuentro, desarrollado con todas las medidas de bioseguridad correspondientes, se trató:
 
1.   La **información sistematizada de todas las personas que han ingresado a la ciudad en los últimos 15 días, provenientes de otros países.** El mapeo incluye la geolocalización de todos los casos que han sido informados y la notificación fehaciente para que se cumpla el aislamiento correspondiente.
2.   La puesta en común de un **Plan de Abordaje Integral de la emergencia para Río Ceballos**, coordinado y validado por todas las fuerzas presentes, que aseguran el cuidado, la vigilancia y la implementación efectiva de medidas en diferentes niveles, para asegurar la prevención, la asistencia Y el acompañamiento de toda la ciudadanía, en el marco de la emergencia sanitaria.
3.   La aprobación por parte del Concejo Deliberante de la **[Ordenanza Nº 2699/2020](https://drive.google.com/file/d/1qonFrELx6JCLVd6OdHlIJjOVR8wtGOXq/view) que destina una partida presupuestaria específica para la Emergencia Sanitaria.**
Todos los presentes coincidieron en la importancia de trabajar de manera conjunta, coordinada y unificada, para que los esfuerzos y las acciones alcancen los mejores resultados y para que toda  la comunidad acompañe con su aporte al cuidado de todos.