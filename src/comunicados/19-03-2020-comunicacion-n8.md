---
title: Comunicación Nº8
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-19T20:00
---

En virtud de la Emergencia Sanitaria Nacional y de las disposiciones nacionales y provinciales tendientes a reducir al máximo la circulación de la población y la exposición a posibles situaciones de contagio del virus COVI-19, el Departamento Ejecutivo Municipal informa las medidas y decretos emitidos en consonancia:  

1. [Decreto Nº 159/2020](https://drive.google.com/file/d/1t8pR8BZizQnrWlHQqSq1ZQYQhECFgBPL/view) (de fecha 18/03/2020), por el cual la Municipalidad de Río Ceballos se adhiere  a lo dispuesto por Decreto Nº 198/2020 de la Provincia de Córdoba y dispone  la suspensión de las actividades de todos los establecimientos hoteleros  o de alojamiento, hasta el día 31 de marzo.

2. [Decreto Nº 162/2020](https://drive.google.com/file/d/1MKT-JsU0Y3-mSPwtMlKjIkNL4jg8BUHE/view) (de fecha 19/03/2020) por el cual se promulga la Ordenanza Nº 2698/2020, sancionada por el Concejo Deliberante mediante la cual la Municipalidad de Río Ceballos adhiere a las disposiciones de la Ley Provincial Nº 10.690, donde la Provincia de Córdoba adhiere a la Emergencia Pública en materia sanitaria declarada por el Estado Nacional.

3. [Decreto Nº 163/2020](https://drive.google.com/file/d/1QZk4D6k71MKUMagWqvpTXHUcDkWEATcq/view) (de fecha 19/03/2020), que dispone el receso administrativo en el ámbito de la Administración Pública Municipal hasta el 31 de Marzo. 
En función de ello el decreto establece que: 
    * Las oficinas y dependencias municipales permanecerán cerradas y sin atención al público.
    * Continuarán en actividad, de acuerdo a las necesidades y condiciones del contexto, las siguientes áreas: 
        * Departamento Ejecutivo Municipal
        * Dirección de Salud Municipal
        * Guardia Urbana Municipal 
        * Área de tránsito (inspectores en la vía pública) de la Secretaria de Gobierno 
        * Servicios Públicos 
    * Todas las áreas deberán adecuar su actividad a un plan de contingencia y guardias mínimas, asegurando la respuesta en casos de urgencias. 
    * Se  destinan todos los recursos presupuestarios que sean necesarios para la implementación de las acciones preventivas y de asistencia. 

4. [Decreto Nº 164/2020](https://drive.google.com/file/d/1mTsm4Kmu1wIWTutjhstBYtoWzrbQL73f/view) (con fecha 19/03/2020) por el cual se prorroga la fecha de vencimiento para el pago de la cuota N°2 de la  tasa por servicio a la propiedad y se establece: 

    * 1° Vto: 03/04/2020
    * 2° Vto: 10/04/2020