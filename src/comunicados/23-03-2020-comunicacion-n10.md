---
title: Comunicación Nº10
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-23T18:00
---

En horas de la mañana se realizó una reunión para la creación de un decreto de aplicación del DNU de Aislamiento Social Preventivo y Obligatorio y la realización de un Proyecto de Ordenanza de Rectificación de Presupuesto 2020. 

**Aplicación del Decreto de Necesidad y Urgencia N° 297 “Aislamiento social y preventivo” del Poder Ejecutivo Nacional de fecha 19 de Marzo del 2020, con vigencia hasta el 31 de marzo inclusive. El mismo dispone:**

**En relación a los lugares  de abastecimiento (supermercados, despensas, carnicerías, pescaderías, verdulerías, panaderías, venta de productos de limpieza  y todo otro que implique  el suministro  de elementos esenciales) la cantidad de personas que se admitirán dentro de los mismos deberá ser limitado a una (1) persona por grupo familiar,** debiendo cada comercio adoptar las medidas necesarias a tales fines permitiendo el ingreso del público al interior del mismo como máximo al doble de números de caja de cobro en funcionamiento.

Que el **horario de atención de los comercios mencionados estará comprendido en el margen comprendido entre las 7 y las 20 hs.**

Que el **servicio de cadetería y/o delivery para reparto a domicilio de alimentos, medicamentos, productos de higiene, de limpieza y otros insumos de necesidad será prestado únicamente por aquellos trabajadores  del rubro  que estén  expresamente autorizados por el D.E.M. entre  las 08:00  y las 22:00 hs.**

**La prohibición de  actividad a los comercios habilitados bajo los rubros “bar, resto-bar, café, restaurante, comedor” y afines.**

**Las farmacias podrán funcionar de 7 a 20 hs. quedando exceptuadas aquellas que deban cumplir con el cronograma de turnos** estipulados por las mismas.

Las **Estaciones de Servicio podrán funcionar las 24 hs. del día** en lo que hace únicamente al expendio de combustible.

El servicio de **sepelios y/o velorios podrá realizarse únicamente en horarios matutinos y vespertinos no podrá superar las dos (2) horas de por turno** y sólo podrán ingresar cónyuges, hermanos, afines en línea recta ascendente y descendiente del occiso. 

**Los taxis deben ser conducidos por un chofer, sin acompañante y llevar como máximo a  dos pasajeros por viaje extremando las medidas de protección e higiene personal** y procurar una pormenorizada limpieza y desinfección del vehículo afectado al servicio antes, durante y después de la jornada laboral.

**Quienes ostenten el rango de funcionario municipal se encuentran a disposición del Departamento Ejecutivo Municipal cuando éste lo considere necesario**, con excepción de aquellos que se encuentren dentro del denominado grupo de riesgo.
 
**Se autoriza a la Secretaría de Economía y Desarrollo Local proceda a  realizar las exenciones y/o condonación en lo que hace a Tasa de Industria y Comercio  a los comercios afectados por la restricción de actividad ordenada.**  

2. **Proyecto de Ordenanza Rectificación de Presupuesto 2020**

**Propone una rectificación en el presupuesto 2020, para crear una partida presupuestaria “EMERGENCIA SANITARIA, SOCIAL Y ECONÓMICA” con el fin de pago exclusivo de los gastos correspondientes en el marco de la emergencia sanitaria.**