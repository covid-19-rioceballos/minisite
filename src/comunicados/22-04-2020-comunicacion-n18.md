---
title: Comunicación Nº18
subtitle: 'Plan de abordaje integran ante la emergencia sanitaria.'
date: 2020-04-22T20:30
---

### RÍO CEBALLOS. COMISIÓN PARA LA EVALUACIÓN DE LA EMERGENCIA SANITARIA COVID–19

Como parte de la actualización de datos que el Municipio realiza regularmente, se informa que en el Sanatorio Privado del Interior se encuentra internada una persona con COVID-19 positivo.

Se trata de un caso que fue derivado desde otra institución a través de la Obra Social, tal como lo establecen los procedimiento en el caso de Clínicas Prestadoras con capacidad de asistencia para pacientes con cobertura social. Es una persona que no reside actualmente en Río Ceballos, contrajo el virus en otra localidad por lo que no se trata de un contagio local. Su traslado fue realizado cumpliendo rigurosamente con todas las medidas de seguridad protocolizadas.

El Municipio no tiene injerencia en dichas medidas, sin embargo tiene la responsabilidad de informar a la comunidad de manera oportuna, realizar la vigilancia epidemiológica y garantizar la asistencia adecuada tanto a pacientes como a la comunidad en general.

El Intendente y el Director de Salud se han contactado con las autoridades de la Institución Sanitaria, visitaron las instalaciones y constataron el cumplimiento efectivo de los protocolos epidemiológicos vigentes, así como la disponibilidad de todos los medios, insumos y equipamiento para mantener un aislamiento seguro.

Como medida adicional de vigilancia epidemiológica se realizará desde la Dirección de Salud en conjunto con el Director del Servicio de Infectología del Sanatorio un esquema de registro y seguimiento del personal asignado a la asistencia del paciente, con procedimientos de control clínico permanente.

Es importante que toda la comunidad mantenga los cuidados y medidas preventivas estipuladas, con la calma y la responsabilidad que este contexto nos exige. Para ello, recurrir a los canales oficiales de información resulta fundamental. El Municipio queda a entera disposición para responder consultas y continuará publicando la actualización de datos.

Estaremos brindando información regularmente y actualizando los datos como se ha realizado hasta ahora.
