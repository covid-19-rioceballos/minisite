---
title: Comunicación Nº15
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-04-12T12:00
---

La Comisión comunica a la comunidad los datos y medidas más recientes: 

1.	**Decreto Municipal N° 191/2020**. De acuerdo a lo dispuesto por el Decreto Nacional 355/2020 que extiende las medidas de aislamiento social preventivo y obligatorio hasta el 26 de abril, el Municipio establece la continuidad del receso administrativo sanitario y todas las disposiciones vigentes. 

2.	**Vigilancia Epidemiológica**
    * El COE central de la Provincia declaró hoy el estado de alerta epidemiológica en Sierras chicas, incluyendo a las localidades de Mendiolaza, Villa Allende, Saldán y La Calera, hasta el cuadrante noroeste de la ciudad de Córdoba.
    * Hasta el 11/04 se han confirmado en la **Provincia de Córdoba 205 casos positivos**.
    * **Departamento Colön**: 43 casos confirmados.
        - Saldan: 28
        - Va Allende: 7
        - Mendiolaza: 2
        - Colonia Caroya: 2
        - La Calera: 1
        - Cerro Azul: 1
        - Salsipuedes: 1
        - Calera: 1

3.	**Medidas Sanitarias**
La alerta epidemiológica implica que se intensificarán las medidas sanitarias.
    * Refuerzo de los controles de circulación por parte de las fuerzas de seguridad, en todo el corredor y también en la localidad de Río Ceballos, así como la implementación de controles sanitarios en dichos puestos.
    * La realización de operativos de testeo que incluirá principalmente a los equipos de salud de instituciones públicas y privadas y a los contactos estrechos de los casos confirmados.

4.	**A toda la comunidad**
El Municipio recuerda que continúa la disposición de aislamiento y que es la medida más efectiva para retrasar y mitigar la propagación del virus. 
Es indispensable que la población se quede en sus hogares, redoblando el esfuerzo que todos vienen realizando, para no derrumbar los resultados obtenidos hasta ahora. 
    * No salgas si no es estrictamente necesario, para hacer compras de primera necesidad o para trabajar en aquellos casos que están autorizados.
    * Para hacer las compras: 
        * Contactate con comercios cercanos a tu vivienda o de confianza y hace tus encargos por whats app para pasar a buscar un pedido listo.
        * Ponete de acuerdo con vecinos y grupos para buscar los insumos que necesitan o retirar los pedidos y así reducir la cantidad de personas que circulan.
    * Al salir usa barbijo en lugares que pueda haber más de 2 o 3 personas. Y alcohol en gel para tus manos cada vez que manipules dinero u objetos que otros te entregan.
    * Si tuviste que salir, al regresar a casa desinfectá la ropa, los objetos y lávate bien las manos o bañate. 
    * Si está necesitando ayuda económica, acompañamiento emocional, realizar consultas o recibir información, comunícate con las líneas telefónicas municipales disponibles:

**Consultas por síntomas febriles o respiratorios**
* 03543 - 457485 / 451087 | Las 24 hs.
* 03543 - 15571868 | De 8 a 22 hs.
* 08001221444

**Postas Sanitarias**
Vacunación, entrega de leche, consultas por otras patologías
* La Quebrada 03543-451313
* San Francisco 03543-450304
* Loza 03543-450303
* Los Vascos 03543-450555

**Equidad y Desarrollo Social**
* 03543 – 458052 / 03543 – 15572682

**Violencia de género**
* 03543 – 15572682 | Lunes a viernes de 8 a 13 hs. 

**Salud Mental**
* 03543 – 15572733 |De 8 a 17 hs.

**Discapacidad**
* 0351 - 156764508 

**Denuncias por incumplimiento del aislamiento**
* GUM: 0351 153934529 las 24 hs.

Informate en [http://coronavirus.rioceballos.gob.ar/](http://coronavirus.rioceballos.gob.ar/)