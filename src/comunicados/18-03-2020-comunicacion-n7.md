---
title: Comunicación Nº7
subtitle: "Plan de abordaje integran ante la emergencia sanitaria."
date: 2020-03-18T09:00
---

## Se informa que el Ejecutivo Municipal en virtud de la Emergencia Sanitaria emitió dos decretos:

1. [Decreto Nº 154/2020](https://drive.google.com/file/d/1hYgJJH3Eo45wtPJQx7IM1xY4w-_SCyBe/view) (con fecha 16/03) que adhiriendo a las disposiciones del Estado Nacional a fin de reforzar las medidas de cuidado, **establece que embarazadas, mayores de 60 años y grupos de riesgo quedan exceptuados de asistir a sus lugares de trabajo**. 

2. [Decreto Nº 157/2020](https://drive.google.com/file/d/1bUwrmHhsUKhdSEEvwqf78WaRizwo5rnf/view) (con fecha 17/03) con el fin de disminuir la circulación de personal el Departamento Ejecutivo establece el **receso administrativo por razones sanitarias hasta el 31 de marzo**. Con esta medida el municipio estará garantizando las prestaciones básicas, con guardias mínimas y rotativas de acuerdo al esquema laboral definido por cada una de las secretarías.

Se recomienda a toda la comunidad realizar sus pagos por medios electrónicos y sus consultas a través del teléfono 451502 - Mesa de Entrada del municipio.